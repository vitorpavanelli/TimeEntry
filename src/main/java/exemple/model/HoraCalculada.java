/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemple.model;

/**
 *
 * @author vitor
 */
public class HoraCalculada {   
    
    private String from;
    
    private String to;
    
    private HoraType type;
    
    public HoraCalculada() {
        
    }
    
    public HoraCalculada(String from, String to, HoraType type) {
        this.from = from;
        this.to = to;
        this.type = type;
    }
    
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public HoraType getType() {
        return type;
    }

    public void setType(HoraType type) {
        this.type = type;
    }
    
    public int getHoraFrom() {
        if (from != null) {
            String[] s = from.split(":");
            return Integer.parseInt(s[0] + s[1]);
        }
        
        return 0;
    }
    
    public int getHoraTo() {
        if (to != null) {
            String[] s = to.split(":");
            return Integer.parseInt(s[0] + s[1]);
        }
        
        return 0;
    }
}
